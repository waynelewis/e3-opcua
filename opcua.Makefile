#
#  Copyright (c) 2018 - 2019  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Monday, October  7 12:19:55 CEST 2019
# version : 0.4.4
#

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS
#include $(where_am_I)/configure/CONFIG_OPCUA_VERSION


## Exclude linux-ppc64e6500
EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

SUPPORT:=opcuaSupport
SUPPORTDBD:=$(SUPPORT)/dbd

APP:=opcuaApp
APPDB:=$(APP)/Db

EXAMPLETOP=../opcua-upstream/exampleTop
EXAMPLETOPDB=$(EXAMPLETOP)/TemplateDbSup/AnyServerDb

#USR_CXXFLAGS_Linux += -std=c++11
#USR_CXXFLAGS += -DUSE_TYPED_RSET


#ifeq ($(T_A),linux-x86_64)
#USR_LDFLAGS += -Wl,--enable-new-dtags
#USR_LDFLAGS += -u opcua_registerRecordDeviceDriver
#USR_LDFLAGS += -L$(E3_MODULES_VENDOR_LIBS_LOCATION)
#USR_LDFLAGS += -L$(where_am_I)/$(SUPPORT)/os/linux-x86_64
#USR_LDFLAGS += -Wl,-rpath,"\$$ORIGIN/../../../../../siteLibs/vendor/$(E3_MODULE_NAME)/$(E3_MODULE_VERSION)"

#LIB_SYS_LIBS += uabasecpp
#LIB_SYS_LIBS += uaclientcpp
#LIB_SYS_LIBS += uapkicpp
#LIB_SYS_LIBS += uastack
#LIB_SYS_LIBS += xmlparsercpp
#LIB_SYS_LIBS += opcualib
#endif

MODULE_LIBS += $(SUPPORT)/os/linux-x86_64/libopcua.so
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libuabasecpp.so
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libuaclientcpp.so
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libuapkicpp.so
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libuastack.so
VENDOR_LIBS += $(SUPPORT)/os/linux-x86_64/libxmlparsercpp.so

DBDS    += $(SUPPORTDBD)/opcua.dbd
#DBDS    += $(OPCUASRC)/opcuaItemRecord.dbd


## Need to define the absolute path, because driver.Makefile
## doesn't know where these files are.
##

#devOpcua.dbd_SNIPPETS += $(where_am_I)$(OPCUASRC)/20_devOpcuaAll.dbd


#opcuaItemRecord$(DEP): $(COMMON_DIR)/devOpcuaVersionNum.h $(COMMON_DIR)/opcuaItemRecord.h $(COMMON_DIR)/devOpcua.dbd

# Module versioning
#EXPANDVARS  += EPICS_OPCUA_MAJOR_VERSION
#EXPANDVARS  += EPICS_OPCUA_MINOR_VERSION
#EXPANDVARS  += EPICS_OPCUA_MAINTENANCE_VERSION
#EXPANDVARS  += EPICS_OPCUA_DEVELOPMENT_FLAG
#EXPANDFLAGS += $(foreach var,$(EXPANDVARS),-D$(var)="$(strip $($(var)))")


#$(COMMON_DIR)/devOpcuaVersionNum.h: $(where_am_I)$(OPCUASRC)/devOpcuaVersionNum.h@
	#$(EXPAND_TOOL) $(EXPANDFLAGS) $($@_EXPANDFLAGS) $< $@

#$(COMMON_DIR)/opcuaItemRecord.h: $(where_am_I)$(OPCUASRC)/opcuaItemRecord.dbd
	#$(DBTORECORDTYPEH) $(USR_DBDFLAGS) -o $@ $<

#$(COMMON_DIR)/devOpcua.dbd: $(devOpcua.dbd_SNIPPETS)
	#$(ASSEMBLE_TOOL) -o $@ $^

# END : $(OPCUASRC)/Makefile


SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
USR_DBFLAGS += -I $(EXAMPLETOPDB)
USR_DBFLAGS += -I ../template

SUBS=$(wildcard $(APPDB)/*-ess.substitutions)
#TMPS+=$(wildcard $(APPDB)/*.template)
#TMPS+=$(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*-ess.db)

db: $(SUBS) $(TMPS)

$(SUBS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@ > $(basename $(@)).db.d
	$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

$(TMPS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@


.PHONY: db $(SUBS) $(TMPS)


vlibs: $(VENDOR_LIBS) $(MODULE_LIBS)

$(MODULE_LIBS):
	$(QUIET)$(SUDO) install -m 755 $@ $(E3_MODULES_INSTALL_LOCATION_LIB)/linux-x86_64/

$(VENDOR_LIBS):
	$(QUIET)$(SUDO) install -m 755 -d $(E3_MODULES_VENDOR_LIBS_LOCATION)/
	$(QUIET)$(SUDO) install -m 755 $@ $(E3_MODULES_VENDOR_LIBS_LOCATION)/
	$(QUIET)$(SUDO) ln -sf $(E3_MODULES_VENDOR_LIBS_LOCATION)/$(notdir $@) $(E3_SITELIBS_PATH)/linux-x86_64/$(notdir $@)

.PHONY: $(VENDOR_LIBS) $(MODULE_LIBS) vlibs



